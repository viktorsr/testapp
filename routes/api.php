<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/users', 'UsersController@index');
Route::post('/user/new', 'UsersController@store');
Route::post('/user/update/{id}', 'UsersController@update');
Route::delete('/user/{id}', 'UsersController@delete');
Route::get('/groups', 'GroupsController@index');
Route::post('/group/new', 'GroupsController@store');
Route::post('/group/update/{id}', 'GroupsController@update');
Route::delete('/group/{id}', 'GroupsController@delete');

