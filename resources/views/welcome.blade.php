<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <title>Laravel</title>
    </head>
    <body ng-app="testApp">
        <div ng-cloak>
            <md-content>
                <md-tabs class="md-primary" md-selected="selectedTab" md-center-tabs md-dynamic-height>
                    <md-tab label="Users">
                        <md-content class="md-padding" ng-controller="UserListController">
                            <h2 class="md-display-1">Users</h2>
                            <div class="md-body-1">
                                <md-content>
                                    <md-list>
                                        <md-list-item class="md-3-line" ng-repeat="user in users">
                                            <div class="md-list-item-text">
                                                <h3>@{{user.name}}</h3>
                                                <p>@{{user.groupName}}</p>
                                            </div>
                                            <md-button ng-click="edit(user, $event)" class="md-raised md-primary">Edit</md-button>
                                            <md-button ng-click="delete(user)" class="md-raised md-warn">Delete</md-button>
                                            <md-divider ng-if="!$last"></md-divider>
                                        </md-list-item>
                                    </md-list>
                                </md-content>
                            </div>
                            <md-button ng-click="add($event)" class="md-fab md-primary mb-add" aria-label="Add">
                                <md-icon md-svg-src="{{ asset('img/icons/baseline-add-24px.svg') }}"></md-icon>
                            </md-button>
                        </md-content>
                    </md-tab>
                    <md-tab label="Groups">
                        <md-content class="md-padding" ng-controller="GroupListController">
                            <h2 class="md-display-1">Groups</h2>
                            <div class="md-body-1">
                                <md-content>
                                    <md-list>
                                        <md-list-item class="md-3-line" ng-repeat="group in groups">
                                            <div class="md-list-item-text">
                                                <h3>@{{group.name}}</h3>
                                            </div>
                                            <md-button ng-click="edit(group, $event)" class="md-raised md-primary">Edit</md-button>
                                            <md-button ng-click="delete(group)" class="md-raised md-warn">Delete</md-button>
                                            <md-divider ng-if="!$last"></md-divider>
                                        </md-list-item>
                                    </md-list>
                                </md-content>
                            </div>
                            <md-button ng-click="add($event)" class="md-fab md-primary mb-add" aria-label="Add">
                                <md-icon md-svg-src="{{ asset('img/icons/baseline-add-24px.svg') }}"></md-icon>
                            </md-button>
                        </md-content>
                    </md-tab>   
                </md-tabs>
            </md-content>
        </div>





        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-animate.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-aria.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular-messages.min.js"></script>

        <!-- Angular Material Library -->
        <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.js"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
