<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Http\Resources\UserCollection;


class UsersController extends Controller
{

    public function index () {
        return new UserCollection(User::All());
        // return User::All();
    }

    public function store (Request $req) {
        $user = new User;
        $user->name = $req->name;
        $user->group_id = $req->group_id;
        $user->save();

    }

    public function update ($id, Request $req) {
        $user = User::find($id);
        $user->name = $req->name;
        $user->group_id = $req->group_id;
        $user->save();
    }

    public function delete($id) {
        User::find($id)->delete();
    }

}
