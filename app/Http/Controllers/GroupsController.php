<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group as Group;

class GroupsController extends Controller
{
    public function index () {
        return Group::All();
    }

    public function store (Request $req) {
        $group = new Group;
        $group->name = $req->name;
        $group->save();

    }

    public function update ($id, Request $req) {
        $group = Group::find($id);
        $group->name = $req->name;
        $group->save();
    }

    public function delete($id) {
        Group::find($id)->delete();
    }
}
