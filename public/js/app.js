var testApp = angular.module('testApp', ['ngMaterial', 'ngMessages']);

testApp.service('getDataService', function ($http) {

    //get users from DB
    this.getUsers = function () {
        var promise = $http.get('http://localhost/api/users').then(function (response) {
            return response.data;
        });
        return promise;
    }

    //get groups from DB
    this.getGroups = function () {
        var promise = $http.get('http://localhost/api/groups').then(function (response) {
            return response.data;
        });
        return promise;
    }

})

testApp.controller('UserListController', function ($scope, $http, $mdDialog, getDataService) {

    getDataService.getUsers().then(function (response) {
        $scope.users = response.data;
    });

    $scope.close = function () {
        $scope.name = '';
        $scope.group = '';
        $scope.error = '';
        $mdDialog.hide();
    }

    //add user
    $scope.add = function (ev) {
        $mdDialog.show({
            clickOutsideToClose: false,
            closeByDocument: false,
            closeByEscape: false,
            scope: $scope,
            preserveScope: true,
            templateUrl: 'js/usersAdd.tmpl.html',
            controller: function DialogController ($scope, $mdDialog) {
                getDataService.getGroups().then(function (data) {
                    $scope.groups = data;
                });
                //add user function
                $scope.addUser = function () {
                    if ($scope.name && $scope.group) {
                        if (!$scope.users.find(ob => ob['name'] === $scope.name)) {
                            $http.post('http://localhost/api/user/new', { 'name': $scope.name, 'group_id': $scope.group })
                                .then(function success (response) {
                                    getDataService.getUsers().then(function (response) {
                                        $scope.users = response.data;
                                        $scope.name = '';
                                        $scope.group = '';
                                        $scope.error = '';
                                    });
                                    $mdDialog.hide();
                                }, function err (response) {

                                });
                        } else {
                            $scope.error = 'User exists!';
                        }
                    } else {
                        $scope.error = 'Enter username and/or choose user group!';
                    }
                }
            },
        });
    };


    //edit user
    $scope.edit = function (user, ev) {
        $mdDialog.show({
            clickOutsideToClose: false,
            closeByDocument: false,
            closeByEscape: false,
            scope: $scope,
            preserveScope: true,
            templateUrl: 'js/userEdit.tmpl.html',
            controller: function DialogController ($scope, $mdDialog) {
                $scope.name = user.name;
                $scope.group = user.group_id;
                getDataService.getGroups().then(function (data) {
                    $scope.groups = data;
                });

                //edit user function
                $scope.editUser = function () {
                    if ($scope.name && $scope.group) {
                        if (!$scope.users.find(ob => ob['name'] === $scope.name)) {
                            $http.post('http://localhost/api/user/update/' + user.id, { 'name': $scope.name, 'group_id': $scope.group })
                                .then(function success (response) {
                                    getDataService.getUsers().then(function (response) {
                                        $scope.users = response.data;
                                        $scope.name = '';
                                        $scope.group = '';
                                        $scope.error = '';
                                        $mdDialog.hide();
                                    });
                                }, function err (response) {

                                });
                        } else {
                            if ($scope.name === user.name) {
                                $http.post('http://localhost/api/user/update/' + user.id, { 'name': $scope.name, 'group_id': $scope.group })
                                    .then(function success (response) {
                                        getDataService.getUsers().then(function (response) {
                                            $scope.users = response.data;
                                            $scope.name = '';
                                            $scope.group = '';
                                            $scope.error = '';
                                            $mdDialog.hide();
                                        });
                                    }, function err (response) {

                                    });
                            } else {
                                $scope.error = 'User name exists!';
                            }
                        }
                    } else {
                        $scope.error = 'Enter username and/or choose user group!';
                    }

                }

            },
        });
    };

    //delete user
    $scope.delete = function (user) {
        var index = $scope.users.indexOf(user);
        $http.delete('http://localhost/api/user/' + user.id)
            .then(function success (response) {
                $scope.users.splice(index, 1);
            }, function err (response) {

            });
    }


});




testApp.controller('GroupListController', function GroupListController ($scope, $http, $mdDialog, getDataService) {


    getDataService.getGroups().then(function (response) {
        $scope.groups = response;
    });

    $scope.close = function () {
        $scope.name = '';
        $scope.groupName = '';
        $scope.error = '';
        $mdDialog.hide();
    }

    //add group
    $scope.add = function (ev) {
        $mdDialog.show({
            clickOutsideToClose: false,
            closeByDocument: false,
            closeByEscape: false,
            scope: $scope,
            preserveScope: true,
            templateUrl: 'js/groupsAdd.tmpl.html',
            controller: function DialogController ($scope, $mdDialog) {
                //add group function
                $scope.addGroup = function () {
                    if ($scope.groupName) {
                        if (!$scope.groups.find(ob => ob['name'] === $scope.groupName)) {
                            $http.post('http://localhost/api/group/new', { 'name': $scope.groupName })
                                .then(function success (response) {
                                    getDataService.getGroups().then(function (response) {
                                        $scope.groups = response;
                                        $scope.name = '';
                                        $scope.groupName = '';
                                        $scope.error = '';
                                        $mdDialog.hide();
                                    });
                                }, function err (response) {

                                });
                        } else {
                            $scope.error = 'Group exist!';
                        }
                    } else {
                        $scope.error = 'Enter group name!';
                    }
                }
            },
        });
    };

    //edit group
    $scope.edit = function (group, ev) {
        $mdDialog.show({
            clickOutsideToClose: false,
            closeByDocument: false,
            closeByEscape: false,
            scope: $scope,
            preserveScope: true,
            templateUrl: 'js/groupEdit.tmpl.html',
            controller: function DialogController ($scope, $mdDialog) {
                $scope.groupName = group.name;
                //edit group function
                $scope.editGroup = function () {
                    console.log(group.id);
                    if ($scope.groupName) {
                        if (!$scope.groups.find(ob => ob['name'] === $scope.groupName)) {
                            $http.post('http://localhost/api/group/update/' + group.id, { 'name': $scope.groupName })
                                .then(function success (response) {
                                    getDataService.getGroups().then(function (response) {
                                        $scope.groups = response;
                                        $scope.groupName = '';
                                        $scope.name = '';
                                        $scope.error = '';
                                        $mdDialog.hide();
                                    });
                                }, function err (response) {

                                });
                        } else {
                            if (group.name === $scope.groupName) {
                                $scope.groupName = '';
                                $scope.name = '';
                                $scope.error = '';
                                $mdDialog.hide();
                            } else {
                                $scope.error = 'group name exists!';
                            }
                        }
                    } else {
                        $scope.error = 'Enter group name!';
                    }
                }
            },
        });
    };

    //delete group
    $scope.delete = function (group) {
        var index = $scope.groups.indexOf(group);
        $http.delete('http://localhost/api/group/' + group.id)
            .then(function success (response) {
                $scope.groups.splice(index, 1);
            }, function err (response) {
                console.log(response)
            });
    }
});